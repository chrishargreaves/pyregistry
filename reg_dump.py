#! python

import sys
import os
import logging
import pyregistry as pyregistry
import argparse
import logging

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Dumps registry hive to text')
    parser.add_argument('path', type=str,
                        help='path to registry file')
    parser.add_argument("--values", help="shows values too", action="store_true")
    args = parser.parse_args()

    if not os.path.exists(args.path):
        logging.error('Path {} not found'.format(args.path))

    registry_instance = pyregistry.RegistryParser(args.path)
    results = registry_instance.get_contents()
    for each in sorted(results, key=lambda x: x['key']):
        print(each['key'])
        if args.values:
            for each_value in each['values']:
                try:
                    print('{}:[{}]\t {}'.format(each['key'], each_value['name'], each_value['data']))
                except UnicodeEncodeError:
                    logging.error('Error decoding value for {}:{}'.format(each['key'], each_value['name']))

