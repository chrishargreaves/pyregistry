# Python 3 Registry Library

## Introduction

pyregistry is a Python 3 based library for offline access to Windows Registry files.

This is work in progress. Use at your own risk.

## Usage

Example code that uses the library is below:


```
#!python

import py_registry

file_pointer = open('../test_data/SYSTEM', 'rb')
registry_instance = py_registry.RegistryParser(file_pointer)
results = registry_instance.get_contents()
for each in results:
    print(each['key'])
    print(each['values'])
    print(each['datetime'])
    print(each['offset'])
```



The get_contents method returns a list. Each of the values in the list consists of a dictionary with three values:

* 'key' - the full path of the key
* 'datetime' - the last updated time (as unix time with decimal)
* 'values' - a list of all the values within that key
* 'offset' - the offset of the nkrecord

Each of the values within the 'values' list is dictionary with five values:

* 'name' - the name of the value
* 'type' - the type of the value
* 'data' - the actual data as a string
* 'raw_data' - the actual data as bytes
* 'offset' - the offset of the vk record

