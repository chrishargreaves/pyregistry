from distutils.core import setup
setup(name='pyregistry',
      version='2.0',
      description='Pure Python registry hive processing',
      author='Chris Hargreaves',
      author_email='chris@hargs.co.uk',
      py_modules=['pyregistry', 'libs.block_mapping'],
      scripts=['reg_dump.py', 'examples/reg_diff.py'],
      )