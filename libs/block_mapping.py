import logging


class block_mapping():

    def __init__(self, file_pointer, block_size):
        self.file_pointer = file_pointer
        self.file_pointer.seek(0) # reset to start of file
        self.block_size = block_size
        logging.debug("set block size to %d" % block_size)
        no_blocks = self.__get_no_blocks(file_pointer)
        logging.debug("no blocks: %d" % no_blocks)
        self.__initialise_block_map(no_blocks)
        logging.debug("block map initialised")
        self.last_offsets_returned_start = 0
        self.last_offsets_returned_stop = 0

    def __len__(self):
        return self.data_len

    def __getitem__(self, index):
        if isinstance(index, slice):
            self.tag_block_read_based_on_offset_min_max(index.start, index.stop)
            self.file_pointer.seek(index.start)
            self.last_offsets_returned = (index.start,index.stop)
            return self.file_pointer.read(index.stop-index.start)
        else:
            self.file_pointer.seek(index)
            self.last_offsets_returned = (index,index)
            return self.file_pointer.read(1)



    # ------------------------
    #           Private
    # ------------------------

    def __get_no_blocks(self, file_pointer):
        no_blocks = 0

        data = file_pointer.read(self.block_size)
        data_len = len(data)
        while len(data) != 0:
            no_blocks += 1
            data = file_pointer.read(self.block_size)
            data_len = data_len + len(data)
        self.data_len = data_len
        return no_blocks

    def __initialise_block_map(self, no_blocks):
        self.read_block_map = []
        self.tag_block_map = []
        for i in range(0,no_blocks):
            self.read_block_map.append("")
            self.tag_block_map.append("")


    def __get_js_function(self):
        data = """"""
        data += """<script>
                        function drawBlock(x, y, colour)
                        {
                            var c=document.getElementById("myCanvas");
                            var ctx=c.getContext("2d");
                            ctx.fillStyle=colour;
                            ctx.fillRect(x,y,5,5);
                        }
                    </script>"""
        return data

    def __get_draw_block_call(self, x, y, colour):
        data = """"""
        data += """<script>
                        drawBlock(%d, %d, "%s")
                    </script>""" % (x, y, colour)
        return data



    def __get_css(self):
        data = """
            <style media="screen" type="text/css">
                #vis_div {
                     float:left;
                     width:510px;
                     height:500px;
                     overflow:scroll;
                }
                #data_div {
                     font-family: "Courier New", monospace;
                     float:left;
                     width:500px;
                     height:500px;
                     overflow:scroll;
                     font-size:50%;
                }

            </style>"""
        return data

    # ------------------------
    #           Public
    # ------------------------

    def tag_last_block_read(self, tag):
        for i in range(self.last_offsets_returned_start, self.last_offsets_returned_stop):
            if self.tag_block_map[i] == "": # only tag it if it isn't tagged already
                self.tag_block_map[i] = tag

    def tag_block_based_on_offset(self, offset, tag):
        block_id = int(offset/self.block_size)
        self.tag_block_map[block_id] = tag

    def tag_block_read_based_on_offset_min_max(self, offset_min,offset_max):
        block_min = int(offset_min/self.block_size)
        block_max = int(offset_max/self.block_size)
        for i in range(block_min, block_max):
            self.read_block_map[i] = 'read'

    def tag_block_based_on_offset_min_max(self, offset_min,offset_max, tag):
        block_min = int(offset_min/self.block_size)
        block_max = int(offset_max/self.block_size)
        for i in range(block_min, block_max):
            if self.tag_block_map[i] == "": # only tag it if it isn't tagged already
                self.tag_block_map[i] = tag

    def get_block_map_as_string(self, format='txt'):
        out_str = ""
        for i, each in enumerate(self.tag_block_map):
            out_str += "%d (@%d): %s\n" % (i,i*self.block_size, each)
        return out_str

    def get_tag(self, block_id):
        if type(block_id) is str:
            block_id = int(block_id)
        return self.tag_block_map[block_id]

    def get_offset(self, block_id):
        if type(block_id) is str:
            block_id = int(block_id)
        return self.block_size * block_id

    def get_content_no_map_update(self, block_id):
        if type(block_id) is str:
            block_id = int(block_id)
        self.file_pointer.seek(self.block_size*block_id)
        data = self.file_pointer.read(self.block_size)
        return data

    def get_content_expanded_no_map_update(self, block_id):
        if type(block_id) is str:
            block_id = int(block_id)
        if self.tag_block_map[block_id] == "":
            i=block_id
            earliest_blank = i
            while i>0 and self.tag_block_map[i] is "":
                earliest_blank = i
                i -= 1
            i=block_id
            latest_blank = i
            while i<len(self.tag_block_map) and self.tag_block_map[i] is "":
                latest_blank = i
                i += 1
            self.file_pointer.seek(self.block_size*earliest_blank)
            data_to_read = (latest_blank - earliest_blank +1) * self.block_size
            data = self.file_pointer.read(data_to_read)
            return data
        else:
            self.file_pointer.seek(self.block_size*block_id)
            data = self.file_pointer.read(self.block_size)
            return data

    def get_block_map_as_html(self):
        data = """<html>"""
        data += "<head>"
        data += self.__get_css()
        data += """<script>
                    function blockClick(id){
                          xmlhttp=new XMLHttpRequest();
                          xmlhttp.onreadystatechange=function()
                            {
                                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                                {
                                    document.getElementById("data_div").innerHTML=xmlhttp.responseText;
                                }
                            }
                        xmlhttp.open("GET","/get_content?id=" + id,true);
                        xmlhttp.send();
                    }
                    </script>"""
        data += "</head>"
        data += """<div id=vis_div>""" + self.get_block_map_as_svg() + """</div>"""
        data += """<div id=data_div></div>"""
        data += "</html>"
        return data


    def get_block_map_as_svg(self):
        data = ""
        max_width = 500
        x = 10
        y = 10
        data += """<svg height=%dpx xmlns="http://www.w3.org/2000/svg" version="1.1">""" % 100000
        for i, each in enumerate(self.tag_block_map):
            if each == "":
                colour = "#000000"
            elif each == 'hbin':
                colour = "#0000FF"
            elif each == 'nk-del':
                colour = "#FF0000"
            elif each == 'vk-del':
                colour = "#FF0000"
            else:
                colour = "#00FF00"

            data += """
                        <circle onclick="blockClick(this.id)" id=block_%d cx="%d" cy="%d" r="5" stroke="black" stroke-width="1" fill="%s" />
            """ % (i,x, y, colour)

            x += 10
            if x >= max_width:
                x = 10
                y += 10

        data += """</svg>"""
        return data