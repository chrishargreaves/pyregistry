import struct
import logging
import re
from libs import block_mapping
import _io



# key_exists(self, key_path)
# value_exists(self, key_path, value)

# get_key(self, key_path)
# get_value(self, key_path, value, raw=False, return_on_no_value='this wont be something asked for')

# list_subkeys(self, key_path)



class RegistryParser():
    
    def __init__(self, registry_file_pointer):
        logging.debug("Processing registry file...")
        block_size_for_tagging = 8

        if isinstance(registry_file_pointer, _io.BufferedReader):
            self.data = block_mapping.block_mapping(registry_file_pointer, block_size_for_tagging)
            self.name = registry_file_pointer.name

        elif isinstance(registry_file_pointer, str):
            f = open(registry_file_pointer, 'rb')
            self.data = block_mapping.block_mapping(f, block_size_for_tagging)
            self.name = registry_file_pointer

        else:
            print('Unsupported type ({})'.format(type(registry_file_pointer)))
            exit(-1)

        self.block_size = 4096
        self.keys = [] # data gets added to keys in ParseNkRecord()
        self.keys_dict = {}  # also add to dict for direct access

        # Baseblock reads 4096 so need at least that 
        if len(self.data)>4096:
            base_block = self.__parse_base_block()
            self.data.tag_block_based_on_offset_min_max(0, 4096, 'baseblock')

            self.first_hbin_pos = self.__get_first_hbin_pos()

            # Tag all the hbin headers
            temp_pos = self.first_hbin_pos
            while temp_pos < len(self.data):
                if self.data[temp_pos:temp_pos+4] == b'hbin':
                    self.data.tag_block_based_on_offset_min_max(temp_pos, temp_pos+8, 'hbin')
                temp_pos += 4096

            # if there is an hbin then start processing, otherwise give up
            if self.first_hbin_pos:
                # the root key is 'root_cell' bytes from first hbin
                root_key_pos = self.first_hbin_pos + base_block["root_cell"]
                current_path = ""
                
                # Start traversing the registry tree...
                self.__parse_record(root_key_pos, current_path, first=True) # This will be an 'nk' record
            else:
                logging.warning("Did not find an hbin (%s)" % registry_file_pointer.name)


        # Have now processed it properly, find and tag deleted
        temp_pos = 0
        while temp_pos < len(self.data):
            if self.data[temp_pos+4:temp_pos+6] == b'nk':
                self.data.tag_block_based_on_offset_min_max(temp_pos, temp_pos+8, 'nk-del')
            if self.data[temp_pos+4:temp_pos+6] == b'vk':
                self.data.tag_block_based_on_offset_min_max(temp_pos, temp_pos+8, 'vk-del')
            temp_pos += 8

        # sort the keys on key name
        self.keys = sorted(self.keys, key=lambda x:x['key'])

        logging.debug("Done...")

    def get_contents(self):
        """Returns the keys and values recovered from the registry hive"""
        return self.keys

    def get_key(self, key_path):
        """Returns the information associated with a particular key"""
        res = self.keys_dict.get(key_path)
        if res:
            return res
        else:
            raise ValueError('Key "{}" not found in {}'.format(key_path, self.name))

    def key_exists(self, key_path):
        """Returns True if key exists, otherwise returns False"""
        res = self.keys_dict.get(key_path)
        if res:
            return True
        else:
            return False

    def get_value(self, key_path, value, raw=False, return_on_no_value='this wont be something asked for'):
        """Returns the information associated with a particular value in a key.
        Returns None if not present

        If raw==True then something like the following is returned:
        {'data': 1, 'offset': 601480, 'type': 'REG_DWORD', 'name': 'Current', 'raw_data': b'\x01\x00\x00\x00'}

        Function can be forced to return if value is not found

        """
        if not self.keys_dict.get(key_path):
            raise ValueError('Key "{}" not found'.format(key_path))
        else:
            for each_value in self.keys_dict.get(key_path)['values']:
                if each_value['name'] == value:
                    if raw:
                        return each_value
                    else:
                        return each_value.get('data')

        if return_on_no_value == 'this wont be something asked for':
            raise ValueError('Value "{}" not found in "{}" in {}'.format(value, key_path, self.name))  # if get to end then value isn't present
        else:
            return return_on_no_value


    def value_exists(self, key_path, value):
        """Returns True if value exists, otherwise returns False"""
        if not self.keys_dict.get(key_path):
            raise ValueError('Key "{}" not found'.format(key_path))
        else:
            for each_value in self.keys_dict.get(key_path)['values']:
                if each_value['name'] == value:
                    return True
        return False


    def list_subkeys(self, key_path):
        """Returns a list of all the subkeys that a key has"""
        matches = []
        pattern = key_path + '/([^/]+)$'

        if key_path not in self.keys_dict:
            raise ValueError("Key '{}' not found.".format(key_path))

        for each_key in self.keys:
            res = re.match(pattern, each_key['key'])
            if res:
                matches.append(res.group(1))
        return matches


    def get_total_allocated_blocks(self):
        total_allocated = 0
        for each in self.data.tag_block_map:
            if each != "":
                total_allocated += 1
        return total_allocated

    def get_total_unallocated_blocks(self):
        total_unallocated = 0
        for each in self.data.tag_block_map:
            if each == "":
                total_unallocated += 1
        return total_unallocated

    def get_total_blocks(self):
        return len(self.data.tag_block_map)

    def __get_first_hbin_pos(self):
        """Identifies the position of the first hbin"""
        pos = 0
        while pos < len(self.data)-4:
            if self.data[pos:pos+4] == b"hbin":
                # found an hbin
                return pos
            else:
                pos = pos + self.block_size
        # if you get to the end and haven't found on return null
        return 

    def __parse_base_block(self):
        """Recover all the values from the baseblock (block 0)"""

        base_block_data = self.data[0:4096]
        
        values = struct.unpack("<IIIQIIIIIII64s396sI3576sII", base_block_data)
        
        base_block = {}
        base_block["signature"] = values[0]
        base_block["sequence1"] = values[1]
        base_block["sequence2"] = values[2]
        base_block["timestamp"] = values[3]
        base_block["major_version"] = values[4]
        base_block["minor_version"] = values[5]
        base_block["type"] = values[6]
        base_block["format"] = values[7]
        base_block["root_cell"] = values[8]
        base_block["length"] = values[9]
        base_block["cluster"] = values[10]
        base_block["filename"] = values[11]
        base_block["reserved1"] = values[12]
        base_block["checksum"] = values[13]
        base_block["reserved2"] = values[14]
        base_block["boot_type"] = values[15]
        base_block["boot_recover"] = values[16]
        
        return base_block

    def __parse_record(self, pos, current_path, first=False):
        """Given a position to a record, this identifies the record type and calls the appropriate parsing code"""
        signature = self.data[pos+4:pos+4+2]
        if len(signature) != 2:
            logging.error("Signature size was not 2 bytes offset %d." % pos)
            return

        if signature == b"nk":
            self.__parse_nk_record(pos, current_path, first)
        elif signature == b"lf":
            self.__parse_lf_record(pos, current_path)
        elif signature == b"lh":
            self.__parse_lh_record(pos, current_path)
        elif signature == b"ri":
            self.__parse_ri_record(pos, current_path)
        elif signature == b"li":
            self.__parse_li_record(pos, current_path)
        elif signature == b"vk":
            self.__parse_vk_record(pos, current_path)
        elif signature == b'sk':
            self.__parse_sk_record(pos, current_path)
        else:
            logging.error("RegParser found a " + str(signature) + " record. Panic.")
            raise RegistryParserException("ERROR: RegParser found a " + str(signature) + " record. Panic.")

    def __get_record_data(self, start, end, type):
        self.data.tag_block_based_on_offset_min_max(start,end,type)
        return self.data[start:end]


    def __parse_sk_record(self, pos, current_path):
        """Parses an Sk Record"""
        sk_record = {}
        size_data = self.data[pos:pos+4]
        if len(size_data) != 4:
            logging.error("Data size was not 4 bytes (supposed to be an Int) offset %d" % pos)
            return

        size = struct.unpack("<i", size_data)[0] # you have to interpret the size as signed int
        if size >= 0:
            raise RegistryParserException("Warning: Was referred to a deleted NK key (positive size value)")
        else:
            sk_record["pos"] = pos # offset into current file
            sk_record["size"] = abs(size)
            sk_data = self.__get_record_data(pos,pos+sk_record["size"], 'sk')

            # More to do here to get rest of details

        return sk_record


    def __parse_nk_record(self, pos, current_path, first=False):
        """Parses an Nk Record"""
        nk_record = {}
        size_data = self.data[pos:pos+4]
        if len(size_data) != 4:
            logging.error("Data size was not 4 bytes (supposed to be an Int) offset %d" % pos)
            return

        size = struct.unpack("<i", size_data)[0] # you have to interpret the size as signed int
        if size >= 0:
            raise RegistryParserException("Warning: Was referred to a deleted NK key (positive size value)")
        else:
            nk_record["pos"] = pos # offset into current file
            nk_record["size"] = abs(size)
            nk_data = self.__get_record_data(pos,pos+nk_record["size"], 'nk')

            fixed_size_data = nk_data[0:80]
            values = struct.unpack("<I2sHQIIIIIIIIIIHBBIIIIHH", fixed_size_data)
            #nk_record["size"] = values[0]
            nk_record["signature"] = values[1]
            #nk_record["flags"] = values[2]
            nk_record["timestamp"] = values[3]
            #nk_record["spare"] = values[4]
            nk_record["parent_offset"] = values[5]
            nk_record["subkeycount"] = values[6]
            nk_record["subkeycount_volatile"] = values[7]
            nk_record["subkeylist_offset"] = values[8]
            nk_record["subkeylist_offset_volatile"] = values[9]
            nk_record["valuelist_count"] = values[10]
            nk_record["valuelist_offset"] = values[11]
            nk_record["security_offset"] = values[12]
            #nk_record["class_offset"] = values[13]
            #nk_record["maxnamelen"] = values[14]
            #nk_record["userflags"] = values[15]
            #nk_record["debug"] = values[16]
            #nk_record["maxclasslen"] = values[17]
            #nk_record["maxvaluenamelen"] = values[18]
            #nk_record["maxvaluedatalen"] = values[19]
            #nk_record["workvar"] = values[20]
            nk_record["namelength"] = values[21]
            #nk_record["classlength"] = values[22]
            
            nk_record["name"] = nk_data[80:80+nk_record["namelength"]].decode("latin-1")

            if first:
                self.reg_root_name = '/' + nk_record['name']

            current_path = current_path + "/" + nk_record["name"] 

            returnable_values = []

            # Go and recover the value list and fetch the values
            if nk_record["valuelist_count"] > 0:
                real_offset = nk_record["valuelist_offset"] + self.first_hbin_pos
                returnable_values = self.__parse_value_list(real_offset, nk_record["valuelist_count"], current_path)

            # Go and recover Sk details
            if nk_record["security_offset"] > 0:
                real_offset = nk_record["security_offset"] + self.first_hbin_pos
                sk_record = self.__parse_record(real_offset, current_path)


            returnable_record = {}
            returnable_record["key"] = current_path
            returnable_record["datetime"] = self.__convert_windows_time(nk_record["timestamp"])
            returnable_record["values"] = returnable_values
            returnable_record["offset"] = pos

            self.keys.append(returnable_record)
            self.__add_to_key_dict(returnable_record)

            #----------------------------------

            # Go and recover the sub keys
            if nk_record["subkeycount"] > 0:
                real_offset = nk_record["subkeylist_offset"] + self.first_hbin_pos
                self.__parse_record(real_offset, current_path)
   
            return

    def __add_to_key_dict(self, record):
        simple_key_path = record['key'].replace(self.reg_root_name, '', 1)
        record['raw_key'] = record['key']
        record['key'] = simple_key_path
        self.keys_dict[simple_key_path] = record



    def __parse_lf_record(self, pos, current_path):
        lf_record = {}
        size_data = self.data[pos:pos+4]
        if len(size_data) != 4:
            logging.error("Data size was not 4 bytes (supposed to be an Int) offset %d" % pos)
            return

        size = struct.unpack("<i", size_data)[0] # you have to interpret the size as signed int
        if size >= 0:
            raise RegistryParserException("WARNING: Was referred to a deleted lf record (positive size value)")
        else:
            # check lf signature
            header_data = self.data[pos:pos+8]
            self.data.tag_block_based_on_offset_min_max(pos, pos+8, 'lf-header')
            values = struct.unpack("<i2sH", header_data)
            signature = values[1]
            if signature != b"lf":
                raise RegistryParserException("WARNING: Expected 'lf' signature but found " + str(signature))
            else:
                no_entries = values[2]

                offsets_to_parse = []
                cur_pos = pos
                
                for i in range(0,no_entries):
                    cur_pos = cur_pos + 8
                    values = struct.unpack("<I4s", self.data[cur_pos:cur_pos+8])
                    self.data.tag_block_based_on_offset_min_max(cur_pos, cur_pos+8, 'lf-record')

                    offset = values[0]
                    hash = values[1]
                    offsets_to_parse.append(offset)
                    
                for each_offset in offsets_to_parse:
                    real_offset = each_offset + self.first_hbin_pos
                    self.__parse_record(real_offset, current_path)
                    
    
    def __parse_lh_record(self, pos, current_path):
        size_data = self.data[pos:pos+4]
        if len(size_data) != 4:
            logging.error("Data size was not 4 bytes (supposed to be an Int) offset %d" % pos)
            return

        size = struct.unpack("<i", size_data)[0] # you have to interpret the size as signed int
        if size >= 0:
            raise RegistryParserException("WARNING: Was referred to a deleted lh record (positive size value)")

        else:
            # check lf signature
            header_data = self.data[pos:pos+8]
            self.data.tag_block_based_on_offset_min_max(pos, pos+8, 'lh_header')
            values = struct.unpack("<i2sH", header_data)
            signature = values[1]
            if signature != b"lh":
                raise RegistryParserException("WARNING: Expected 'lh' signature but found " + str(signature))

            else:
                no_entries = values[2]
                
                offsets_to_parse = []
                cur_pos = pos
                
                for i in range(0,no_entries):
                    cur_pos = cur_pos + 8
                    values = struct.unpack("<I4s", self.data[cur_pos:cur_pos+8])
                    self.data.tag_block_based_on_offset_min_max(cur_pos, cur_pos+8, 'lh-record')
                    offset = values[0]
                    hash = values[1]
                    #Logging.log("subkey entry in lh " + str(cur_pos) + " " + str(offset) +  " " + hash)
                    offsets_to_parse.append(offset)
                    
                for each_offset in offsets_to_parse:
                    real_offset = each_offset + self.first_hbin_pos
                    #Logging.log("Calling ParseRecord from lhrecord at " + str(pos))
                    self.__parse_record(real_offset, current_path)



    def __parse_ri_record(self, pos, current_path):
        #Logging.log("-------------------------------------------")
        #Logging.log("parsing ri record at offset " + str(pos))
        size_data = self.data[pos:pos+4]
        if len(size_data) != 4:
            logging.error("Data size was not 4 bytes (supposed to be an Int) offset %d" % pos)
            return

        size = struct.unpack("<i", size_data)[0] # you have to interpret the size as signed int
        if size >= 0:
            raise RegistryParserException("WARNING: Was referred to a deleted ri record (positive size value)")
        else:
            # check lf signature
            header_data = self.data[pos:pos+8]
            self.data.tag_block_based_on_offset_min_max(pos, pos+8, 'ri-header')
            values = struct.unpack("<i2sH", header_data)
            signature = values[1]
            if signature != b"ri":
                raise RegistryParserException("WARNING: Expected 'ri' signature but found " + str(signature))
            else:
                no_entries = values[2]
                #Logging.log("found " + str(no_entries) + " subkeys in ri")
                
                offsets_to_parse = []
                cur_pos = pos + 8 # in ri you need to skip 4 (header is 8 but entries are 4)
                
                for i in range(0,no_entries):
                    values = struct.unpack("<I", self.data[cur_pos:cur_pos+4])
                    self.data.tag_block_based_on_offset_min_max(cur_pos, cur_pos+4, 'ri-record')
                    offset = values[0]
                    #Logging.log("subkey entry in ri @" + str(cur_pos) + " -> " + str(offset))
                    offsets_to_parse.append(offset)
                    cur_pos = cur_pos + 4
                    
                for each_offset in offsets_to_parse:
                    real_offset = each_offset + self.first_hbin_pos
                    #Logging.log("Calling ParseRecord from rirecord at " + str(pos))
                    self.__parse_record(real_offset, current_path)
        return

    def __parse_li_record(self, pos, current_path):
        #Logging.log("-------------------------------------------")
        #Logging.log("parsing ri record at offset " + str(pos))
        size_data = self.data[pos:pos+4]
        if len(size_data) != 4:
            logging.error("Data size was not 4 bytes (supposed to be an Int) offset %d" % pos)
            return

        size = struct.unpack("<i", size_data)[0] # you have to interpret the size as signed int
        if size >= 0:
            raise RegistryParserException("Was referred to a deleted li record (positive size value)")
        else:
            # check lf signature
            header_data = self.data[pos:pos+8]
            self.data.tag_block_based_on_offset_min_max(pos, pos+8, 'li-header')
            values = struct.unpack("<i2sH", header_data)
            signature = values[1]
            if signature != b"li":
                raise RegistryParserException("Expected 'li' signature but found " + str(signature))
            else:
                no_entries = values[2]
                #Logging.log("found " + str(no_entries) + " subkeys in ri")
                
                offsets_to_parse = []
                cur_pos = pos + 8 # in ri you need to skip 4 (header is 8 but entries are 4)
                
                for i in range(0,no_entries):
                    values = struct.unpack("<I", self.data[cur_pos:cur_pos+4])
                    self.data.tag_block_based_on_offset_min_max(cur_pos, cur_pos+4, 'li-record')
                    offset = values[0]
                    #Logging.log("subkey entry in ri @" + str(cur_pos) + " -> " + str(offset))
                    offsets_to_parse.append(offset)
                    cur_pos = cur_pos + 4
                    
                for each_offset in offsets_to_parse:
                    real_offset = each_offset + self.first_hbin_pos
                    self.__parse_record(real_offset, current_path)
        return


    def __parse_value_list(self, pos, no_entries, current_path):
        #Logging.log("-------------------------------------------")
        #Logging.log("parsing value list at offset " + str(pos))
        returnable_values = []
        size_data = self.data[pos:pos+4]
        self.data.tag_block_based_on_offset_min_max(pos, pos+8, 'valuelist_header')
        if len(size_data) != 4:
            logging.error("Data size was not 4 bytes (supposed to be an Int) offset %d" % pos)
            return []
        size = struct.unpack("<i", size_data)[0] # you have to interpret the size as signed int
        if size >= 0:
            raise RegistryParserException("WARNING: Was referred to a deleted value list (positive size value)")
        else:
            # no signature to check here
            cur_pos = pos + 4 # skip over size value in header
            for i in range(0,no_entries):
                off_data = self.data[cur_pos:cur_pos+4]
                self.data.tag_block_based_on_offset_min_max(cur_pos, cur_pos+4, 'valuelist_entry')
                each_offset = struct.unpack("<I", off_data)[0]
                real_offset = each_offset + self.first_hbin_pos
                #Logging.log("Calling ParseRecord from value list at " + str(pos))
                fetched_values = self.__parse_vk_record(real_offset, current_path)
                returnable_values.append(fetched_values)
                cur_pos = cur_pos + 4
        
        return returnable_values




    def __parse_vk_record(self, pos, current_path):
        #Logging.log("-------------------------------------------")
        #Logging.log("parsing vk record at offset " + str(pos))
        vk_record = {}
        size_data = self.data[pos:pos+4]
        if len(size_data) != 4:
            logging.error("Data size was not 4 bytes (supposed to be an Int) offset %d" % pos)
            return {}

        size = struct.unpack("<i", size_data)[0] # you have to interpret the size as signed int
        if size >= 0:
            raise RegistryParserException("WARNING: Was referred to a deleted Vk Record  (positive size value)")
        else:
            vk_record["size"] = abs(size)
            vk_data = self.__get_record_data(pos,pos+vk_record["size"],'vk')

            vk_record["name_length"] = struct.unpack("<H", vk_data[6:8])[0]
            vk_record["data_length"] = struct.unpack("<I", vk_data[8:12])[0]
            vk_record["type_code"] = struct.unpack("<I", vk_data[16:20])[0]
            vk_record["type"] = self.__get_data_type(vk_record["type_code"])
            vk_record["flags"] = struct.unpack("<H", vk_data[20:22])[0]
            
            if vk_record["flags"] & 0x0000000000000001:
                # if bit 0 is set then we have a compressed name (ASCII, otherwise it's unicode)
                vk_record["name"] = vk_data[24:24+vk_record["name_length"]].decode("latin-1")
            else:
                vk_record["name"] = vk_data[24:24+vk_record["name_length"]].decode("UTF-16")

            if vk_record["name_length"] == 0:
                vk_record["name"] = "Default"
            
            #Logging.log("data_len_bin " + bin(vk_record["data_length"]))
            # if length is less than 4 or bit 31 of length is set then data is stored within the key
            if vk_record["data_length"] <= 4 or vk_record["data_length"] & 10000000000000000000000000000000:
                # data is contained within key
                vk_record["data"] = vk_data[12:16]
                vk_record["data_length"] = 4
                # print "---" + vk_record["name"] + " " + vk_record["data"]
            else:
                # follow the offset for the data
                vk_record["data_offset"] = struct.unpack("<I", vk_data[12:16])[0]
                data_pos = vk_record["data_offset"] + self.first_hbin_pos

                vk_record["data"] = self.__parse_data_node(data_pos, current_path)
                vk_record["data"] = vk_record["data"][0:vk_record["data_length"]] # make sure only actual data is captured

            # Different key types need to be interpreted in different ways
            vk_record['interpreted_data'] = self.get_data_interpreted_per_type(vk_record['data'], vk_record['type'])

            returnable_value = dict()
            returnable_value["name"] = vk_record["name"]
            returnable_value["type"] = vk_record["type"]
            returnable_value["data"] = vk_record["interpreted_data"]
            returnable_value["raw_data"] = vk_record["data"]
            returnable_value["offset"] = pos

            return returnable_value

    def get_data_interpreted_per_type(self, data, data_type):
        """Return the correct interpreted data based on the REG type"""
        if data_type == "REG_NONE":
            return self.__print_hex(data)
        elif data_type == "REG_SZ" or data_type == "REG_EXPAND_SZ" or data_type == "REG_MULTI_SZ":
            try:
                return_data = data.decode("UTF-16-LE", errors="ignore")
                check_for_null = return_data.find('\x00') # this shouldn't be in a string. Some registry values are strings but have stray binary data at the end (see unit tests)
                if check_for_null != -1:
                    return_data = return_data[:check_for_null]
                else:
                    return_data = return_data
                return_data = return_data.rstrip("\x00") # gets rid of any stray unicode
            except UnicodeDecodeError:
                # raise RegistryParserException("Unicode conversion error with " + PrintHex(vk_record["data"]) + " len=" + str(len(vk_record["data"])))
                logging.warning("Unicode conversion error with " + self.__print_hex(data) + " len=" + str(len(data)))
                return_data = self.__print_hex(data)
            return return_data
        elif data_type == "REG_BINARY":
            return data
        elif data_type == "REG_DWORD" or data_type == 'REG_DWORD_LITTLE_ENDIAN':
            if len(data) == 4:
                res = struct.unpack('<i', data)
                return res[0]
            else:
                logging.warning('[WARNING] {}-byte DWORD ({})'.format(len(data), self.__print_hex(data)))
                return -1
        elif data_type == 'REG_DWORD_BIG_ENDIAN':
            if len(data) == 4:
                res = struct.unpack('<i', data)
                return res[0]
            else:
                logging.warning('[WARNING] {}-byte DWORD (BE) ({})'.format(len(data), self.__print_hex(data)))
                return -1
        elif data_type == "REG_QWORD" or data_type == "REG_QWORD_LITTLE_ENDIAN":
            if len(data) == 8:
                res = struct.unpack('<Q', data)
                return res[0]
            else:
                logging.warning('[WARNING] {}-byte QWORD ({})'.format(len(data), self.__print_hex(data)))
                return -1
        else:
            return self.__print_hex(data)

    def __get_data_type(self, data_type):
        """Returns data type for given code. See Carvey Registry book p31"""
        if data_type == 0:
            return "REG_NONE"
        elif data_type == 1:
            return "REG_SZ"
        elif data_type == 2:
            return "REG_EXPAND_SZ"
        elif data_type == 3:
            return "REG_BINARY"
        elif data_type == 4:
            return "REG_DWORD"
        elif data_type == 5:
            return "REG_DWORD_BIG_ENDIAN"
        elif data_type == 6:
            return "REG_LINK"
        elif data_type == 7:
            return "REG_MULTI_SZ"
        elif data_type == 8:
            return "REG_RESOURCE_LIST"
        elif data_type == 9:
            return "REG_FULL_RESOURCE_DESCRIPTOR"
        elif data_type == 10:
            return "REG_RESOURCE_REQUIREMENTS_LIST"
        elif data_type == 11:
            return "REG_QWORD"
        elif data_type == 12:
            logging.error('Unknown type 12')
            return "?"
        elif data_type == 13:
            logging.error('Unknown type 12')
            return "?"
        
        else:
            return str(data_type) 
            
    def __parse_data_node(self, pos, current_path):
        data_node = {}
        size_data = self.data[pos:pos+4]
        if len(size_data) != 4:
            logging.error("Data size was not 4 bytes (supposed to be an Int) offset %d" % pos)
            return

        size = struct.unpack("<i", size_data)[0]
        data_node["size"] = abs(size)
        node_data = self.data[pos:pos+data_node["size"]]
        self.data.tag_block_based_on_offset_min_max(pos,data_node["size"], 'data')
        data_node["data"] = node_data[4:4+data_node["size"]-4]
        return data_node["data"]

    def __print_hex(self, data):
        """Prints the hex of the given data"""
        astring = ""
        for each in data:
            astring = astring + ("%02x" % each)
        return astring

    def __convert_windows_time(self, time_to_convert):
        """Converts a Windows time to Unix time unless timestamp is 0, then it returns 0"""
        if time_to_convert <= -1:
            logging.error("Cannot convert negative WindowsTime")
            raise Exception("Cannot convert negative WindowsTime")
        if time_to_convert == 0:
            return 0
        else:
            timestamp_data = time_to_convert - 116444736000000000
            unix_timestamp = timestamp_data/10000000
            return unix_timestamp




class RegistryParserException(Exception):
    """Exception in Registry Parser"""
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


