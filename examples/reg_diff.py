#! python

import pyregistry as pyregistry
import argparse
import logging
import os

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Compares two registry hives')
    parser.add_argument('path1', type=str,
                        help='path to registry file 1')
    parser.add_argument('path2', type=str,
                        help='path to registry file 2')

    args = parser.parse_args()

    if not os.path.exists(args.path):
        logging.error('Path 1 {} not found'.format(args.path))
        exit(-1)

    if not os.path.exists(args.path):
        logging.error('Path 2 {} not found'.format(args.path))
        exit(-1)

    file_pointer1 = open(args.path, 'rb')
    registry_instance1 = pyregistry.RegistryParser(file_pointer1)
    results1 = registry_instance1.get_contents()

    file_pointer2 = open(args.path, 'rb')
    registry_instance2 = pyregistry.RegistryParser(file_pointer2)
    results2 = registry_instance2.get_contents()


    print('Deleted Keys')
    print('------------')
    for each in sorted(results1, key=lambda x: x['key']):
        if each not in results2.keys_dict:
            print(each['key'])


    print('Added Keys')
    print('------------')
    for each in sorted(results2, key=lambda x: x['key']):
        if each not in results1.keys_dict:
            print(each['key'])


        # print(each['key'])
        # print(each['values'])
        # print(each['datetime'])
        # print(each['offset'])
